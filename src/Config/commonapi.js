 import axios from "axios";

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImV4YW1wbGVAZ21haWwuY29tIiwiX2lkIjoiNjIzYWExOWM1OGQ4ZjNkNjY0NTNhODMwIiwiaWF0IjoxNjQ4MDEwNDY1LCJleHAiOjE2NDgwOTY4NjV9.5sx18PmtGWbnBNdOLAu2yGm8HPkv_7znvUw9-fEeRPQ"
const apiUrl = 'http://94.237.3.78:4000/api'

const authAxios = axios.create({
  baseURL: apiUrl,
  headers: {
    Authorization: `Bearer ${token}`,
  }
});

authAxios.get('/get_course')
authAxios.get('/user_Detail/6242d889c8b03dd187589707')
authAxios.post('/createUser')
authAxios.post('/create_course')
authAxios.get('/All_category')
authAxios.put('/Update_course/624148dd95d05783c88401e1') //update_course
 authAxios.post('/create_category_new') 
 authAxios.get('/user_Detail') 
//admin dashboard page ..
authAxios.put('/user_update/') //user_update
authAxios.delete('/user_Deleted') //delete_user 
authAxios.delete('/Delete_course/6245a720b27d190f37dbec06')
// authAxios.post('cart')
// authAxios.post('/student_Enroll')

export const api = `${apiUrl}/login`; // login page 
export const getCourse = `${apiUrl}/get_course`; // on banner page, or selflearning page 
export const createUser = `${apiUrl}/createUser`; //on registeration page 
export const user_Detail = `${apiUrl}/user_Detail`;  //on specific id (my profile page )
export const create_course = `${apiUrl}/create_course`;  //on AddCourse page 
export const AllCategory = `${apiUrl}/All_category`;  //on admin dashboard (sidebar) page
export const UpdateCourse = `${apiUrl}/Update_course`;  //on admin dashboard  specific card page
export const CreateCategory = `${apiUrl}/create_category_new`; // for add new category on sidebar (admin dashboard page )
export const UpdateUser = `${apiUrl}/user_update/`; // for update user from( (admin dashboard page )
export const deleteUser = `${apiUrl}/user_Deleted`; // for delete user from(admin dashboard page )
export const DeleteCourse = `${apiUrl}/Delete_course`;  //on admin dashboard page delete course