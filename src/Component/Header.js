import React from 'react'
 import Test from '../Routing/Test'
import Banner from './Banner'

export default function Header() {
  return (
    <div>
  
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark p-3">
        {/* <nav class="navbar fixed-top navbar-light bg-light"> */}
        {/* .navbar-expand{-sm|-md|-lg|-xl|-xxl} */}
  <div class="container-fluid ">
  <svg xmlns="http://www.w3.org/2000/svg" width="55" height="45"  fill="currentColor" class="navbar-brand bi bi-journal-code" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M8.646 5.646a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L10.293 8 8.646 6.354a.5.5 0 0 1 0-.708zm-1.292 0a.5.5 0 0 0-.708 0l-2 2a.5.5 0 0 0 0 .708l2 2a.5.5 0 0 0 .708-.708L5.707 8l1.647-1.646a.5.5 0 0 0 0-.708z"/>
  <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
  <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
</svg>
   
    {/* <a class="navbar-brand mr-5" href="/banner">COURSE</a> */}
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/">HOME</a>
        </li>
        <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="/self">SELF LEARNING</a>
        </li>
        <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="#">NEWS</a>
        </li>
        <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="#">CONTACT</a>
        </li>
        {/* <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="/registration">Register</a>
        </li> */}
      </ul>
      <form class="d-flex float-right ml-5">
        <input class="form-control me-2 float-right " type="search" placeholder="Search" aria-label="Search" />
        <a class="btn btn-outline-success ml-1" href="/login" type="submit">ACCOUNT</a>
        <span>&nbsp; &nbsp;</span>       <span>&nbsp; &nbsp;</span>
        <a class="nav-link active text-white" aria-current="page" href="/banner" type="submit">LOGOUT </a>
      </form>
    
    </div>
  </div>

</nav>
<Test />
{/* <Banner /> */}
  
   
    </div>
  
  )
}
