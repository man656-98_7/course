import React, { Component } from 'react'
import axios from 'axios'
import '../Assest/Add.css';
import Footer from './Footer';
import { UpdateCourse } from '../Config/commonapi';

class UpdateCours extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: [],
      userId: '',
    }
  }

  saveFile(e) {
    this.setState({ image: e.target.files[0] })
  }
  handleSubmit = e => {

    e.preventDefault();
    let formData = new FormData();
    const data = {
      Title: this.Title,
      Description: this.Description,
      Image: this.state.image,
      duration: this.duration,
      cost: this.cost,
      author: this.author,
      userId: this.userId,
      //cat_id: this.cat_id,
     

    }
   
    for (var key in data) {
      //console.log(key, "Gggg", data[key])
      formData.append(key, data[key])
    }
    let userId;
    alert(userId);
    console.log(">>>>>>>>>>>>",userId)
    axios.put(`Update_course/${userId}`, formData).then(
     
     // http://94.237.3.78:4000/api/Update_course/6242bbdc6ac957629bc9fb9d
      res => {
        console.log(res)
        console.log(">>>>>>>>>>>>",userId)
      }
    ).catch(
      err => {
        console.log(err.response.data.message, "ffff")
      }
    )
  }

  render() {
    console.log(UpdateCourse, "UpdateCourse")

    return (

      <div>
        <form className='m-5 text-center pt-5' onSubmit={this.handleSubmit}>
          <div className='container mx-auto'>

            <div className='form-group'>
              <br></br>

              <label>Title</label>

              <div class="form-group">

                <select class="form-control" id="sel1"
                  onChange={e => this.Title = e.target.value} >
                  <option value="JAVA">JAVA</option>
                  <option value="REACT"> REACT</option>
                  <option value="SCIENCE">SCIENCE</option>
                  <option value="MATHEMATICE">MATHEMATICE</option>
                  <option value="PCM">PCM</option>
                  <option value="CHEMISTRY">CHEMISTRY</option>
                  <option value="BIOLOGY">BIOLOGY</option>
                </select>
              </div>
             

            </div>
            <div className='form-group'>
              <br></br>

              <label>Description</label>
              <input type="text" placeholder='Enter Description' className='form-control'
                onChange={e => this.Description = e.target.value} />

            </div>
            <br></br>
            <div className='form-group'>

              <label>Image</label>
              <input type="file" onChange={(e) => this.saveFile(e)} name="image" />

            </div>
            <br></br>
            <div className='form-group'>
              <label>duration</label>
              <input type="text" placeholder='Enter duration' className='form-control'
                onChange={e => this.duration = e.target.value} />

            </div>
            <br></br>
            <div className='form-group'>
              <label>cost</label>
              <input type="number" placeholder='Enter cost' className='form-control'
                onChange={e => this.cost = e.target.value} />
            </div>
            <br></br>
            <div className='form-group'>
              <label>author</label>
              <input type="text" placeholder='Enter author' className='form-control'
                onChange={e => this.author = e.target.value} />
            </div>
            <br />
            

            <br></br>
            <div className='pt-3'>
              <button className="btn btn-primary ">UpdateCourse .!</button>
           </div>
          </div>
        </form>
        {/* <Footer /> */}
      </div>
    )
  }
}
export default UpdateCours;

