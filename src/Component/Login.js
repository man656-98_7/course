
import React, { Component } from 'react'
import axios from 'axios'
import '../Assest/Login.css';
import Footer from './Footer';
import { api } from '../Config/commonapi';

export default class Login extends Component {
    handleSubmit = e => {
        e.preventDefault();
        const data = {
            email: this.email,
            password: this.password,

        }
        console.log('it works', data);

        axios.post('/login', data).then(

            res => {
                localStorage.setItem("token", res.data.token);
                console.log(res)
            }
        ).catch(
            err => {
                console.log(err)
            }
        )
    }

    render() {

        console.log(api, "api")
        return (
            <div>
                <form className='m-5  pt-5' onSubmit={this.handleSubmit}>
                    <h4>Welcome To Account Page</h4>
                    <div className='container mx-auto'>
                        <img src="https://images-platform.99static.com//KlBLMX8dQrcq6hZGnxf5HSnG29I=/8x543:525x1060/fit-in/500x500/99designs-contests-attachments/123/123360/attachment_123360235" alt="iugiu" style={{ width: "100px", textAlign: "center" }} />
                        <br></br>
                        <div class="dropdown mt-4">
                            <button type="button" class="dropdown-menu-dark dropdown-toggle" data-toggle="dropdown">
                                LOGIN WITH ..
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Admin</a>
                                <a class="dropdown-item" href="#">User</a>

                            </div>
                        </div>
                        <br></br>
                        <div className='form-group text-center'>
                            <label>Email</label>
                            <input type="text" placeholder='Enter Email' className='form-control'
                                onChange={e => this.email = e.target.value} required />

                        </div>
                        {/* <br></br> */}
                        <div className='form-group'>
                            <label>Password</label>
                            <input type="password" placeholder='Password' className='form-control'
                                onChange={e => this.password = e.target.value} required />
                        </div>
                        {/* <br></br> */}


                        <a href="#" class="card-link text-dark float-right">Forget Password ?</a>
                        <br></br>
                        <br></br>
                        <div className='mx-auto'>
                            <button className="btn btn-primary text-center" href="/banner">LOGIN .!</button>
                          
                        </div>
                        <a class="nav-link active float-right " href="/registeration" type="submit"> Don't hava an account ? Sign Up</a>
                        {/* <span>&nbsp;</span>  <span>&nbsp; &nbsp;  &nbsp;</span> */}
                        </div>
                </form>


                {/* <Footer /> */}

            </div>
        )
    }
}
