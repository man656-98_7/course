import React, { useState, useEffect } from 'react'
import axios from 'axios';
import '../Assest/Selflearning.css';
import Footer from './Footer';
import { getCourse } from '../Config/commonapi';
import Test from '../Routing/Test';


function SelfLearning() {
  const [data, setData] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);
  console.log(data, "fff")
  async function fetchData() {
    await axios.get('/get_course')
      .then((response) => { setData(response.data) })
    //.then((response) => { console.log(response.data) });
  }  
  console.log(getCourse,"getCourse")
  return (
    <div>
      <div className='container'>
       <h1> SelfLearning</h1> 
      </div>
      <div class="sidebar">
    <h2 className='text-white pt-5 bg-primary font-weight-bolder font-italic text-break text-uppercase text-capitalize'>Category</h2>    
  {/* <a class="active" href="#home">Home</a> */}
  {/* <a href="#news">Add Student</a> */}
   {/* <a href="/add">Add Course</a> */}
  {/* <a href="#course">View Category</a> */}
  <a href="/profile">MyProfile</a>
  <a href="/admindash">AdminDashboard</a>
  {/* <a class="nav-link active ml-4" aria-current="page" href="/banner">Home</a> */}

 
</div>


<div class="content">
<h4 class="pt-5 font-italic text-dark">Here are the List of all Courses..</h4>
<div class="container  text-center">
     

    <div class="row row-cols-1 row-cols-md-3 mt-3 mb-5 g-4">
        
          {
          data.map(item => {
            console.log("gdghdfgsgf", data)
            return (
              <div class="col">
              <div class="card">
                <div class="card-img-top">
                  {/* show image from server side */}
                  <img src={`http://94.237.3.78/demo_beta/${item.Image}`} alt="fftgh" style={{width:"345px", height:"300px"}}/>
                  </div>

                <div class="card-body">
                <h6 class="card-title">{item.Title}</h6>
                <p class="card-text">{item.Description}</p>
                <h6>{item.duration}</h6>
                <h6>{item.cost}</h6>
                <p className='pl-0 text-muted'>{item.author}</p>
               </div>
                <div class="mw-100 bg-primary card-footer">
               
                <a href="#" class="btn btn-primary ">Enroll</a>
                 </div>
            
                 </div>
              </div>

            )
          })
        }
       </div>

       

        {/* <div class="row row-cols-1 row-cols-md-3  mb-5 g-4">
          <div class="col">
            <div class="card h-100">
              <img src="https://elearningindustry.com/wp-content/uploads/2015/10/6-convincing-reasons-take-elearning-course.jpg" class="card-img-top" alt="edsr" />
              <div class="card-body">
               
                <p class="card-text">Use practical strategies to communicate effectively online</p>
                <p className='pl-0 text-muted'>By Mr.John</p>
                <h6>$120</h6>
                </div>
                <div class="mw-100 bg-primary card-footer">
                <a href="#" class="btn btn-primary ">Enroll</a>
                </div>


            </div>
          </div> 

         <div class="col">
            <div class="card h-100">
              <img src="https://elearningindustry.com/wp-content/uploads/2015/10/6-convincing-reasons-take-elearning-course.jpg" class="card-img-top" alt="edsr" />
              <div class="card-body">
             
                <p class="card-text">Use practical strategies to communicate effectively online</p>
                <p className='pl-0 text-muted'>By Mr.John</p>
                <h6>$120</h6>
                </div>
                <div class="mw-100 bg-primary card-footer">
                <a href="#" class="btn btn-primary">Enroll</a>
                </div>


            </div>
          </div> 

           <div class="col">
            <div class="card h-100">
              <img src="https://elearningindustry.com/wp-content/uploads/2015/10/6-convincing-reasons-take-elearning-course.jpg" class="card-img-top" alt="edsr" />
              <div class="card-body">
                {/* <h6 class="card-title">Step By Step Explanation</h6 > */}
                {/* <p class="card-text">Use practical strategies to communicate effectively online</p>
                <p className='pl-0 text-muted'>By Mr.John</p>
                <h6>$120</h6>
                </div>
                <div class="mw-100 bg-primary card-footer">
                <a href="#" class="btn btn-primary ">Enroll</a>
                </div>

            </div>
          </div> 
        </div>  */}
 </div>
 <Footer />
 </div>
 {/* <Test /> */}
</div>

 
  )
}
export default SelfLearning;

