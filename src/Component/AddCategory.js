import React, { Component } from 'react'
import axios from 'axios';
import Footer from './Footer';
import '../Assest/UserDetail.scss';
import { CreateCategory } from '../Config/commonapi';


class AddCategory extends Component {

    handleSubmit = e => {
        e.preventDefault();
        const data = {
            name: this.name,
        }
        console.log('it works', data)

        axios.post('/create_category_new', data).then(


            res => {

                console.log(res)
            }
        ).catch(
            err => {
                console.log(err)
            }
        )
    }



    render() {
        console.log(CreateCategory, " creCreateCategoryateUser")
        return (
            <div>

                <form className=' text-center pt-5' onSubmit={this.handleSubmit}>
                    <div className='container mx-auto pt-5' >


                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                            ADD NEW CATEGORY
                        </button>


                        <div class="modal pt-5 pb-5" style={{ paddingBottom: "200px" }} id="myModal">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <h4 class="modal-title">ADD NEW CATEGORY ..</h4>
                                        <button class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <div class="modal-body">

                                        <input type="text" placeholder='Enter Name' className='form-control'
                                            onChange={e => this.name = e.target.value} />


                                    </div>


                                    <div class="modal-footer pb-5">
                                        <button class="btn btn-primary " data-dismiss="modal">SUBMIT</button>

                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </form>
                {/* <Footer /> */}

            </div>
        )

    }
}
export default AddCategory;
