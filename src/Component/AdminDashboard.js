import React, { useState, useEffect } from 'react'
import axios from 'axios';
//import '../Assest/Selflearning.css';
import Footer from './Footer';
import AddCategory from './AddCategory';
import { AllCategory, getCourse, CreateCategory ,DeleteCourse} from '../Config/commonapi';

function AdminDashboard() {
  const [data, setData] = useState([]);
  const [list, setList] = useState([]);
  const [name, setName] = useState("");

//   const handleSubmit = (e) => {
//     e.preventDefault();
//  };
 useEffect(() => {
    fetchData();
    demo();

  }, []);
  //console.log(data, "data")
  async function fetchData() {
    await axios.get('/All_category')
      .then((response) => { setData(response.data) })
    // .then((response) => { console.log(response.data) });
  }

  // console.log(list, "list")
  async function demo() {
    await axios.get('/get_course')
      .then((response) => { setList(response.data) })
   // .then((response) => { console.log(response.data) });

  }


  function coursedelete(id) {
    //  e.preventDefault();
    alert("this is id ");
   // console.log(item.id)
     console.log('The link was clicked.',id);

     fetch(`/Delete_course/${id}`, {
         method: 'DELETE',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json'
         },
     })

 }



console.log(DeleteCourse,"DeleteCourse")
  return (
    <div>
      <div className='container pt-4'>
     </div>
      <div class="sidebar">

        <h2 className='text-white pt-5  font-weight-bolder bg-primary text-break text-uppercase text-capitalize'>Category</h2>

        {/* <AddCategory /> */}
        {
          data.map(item => {
            //console.log(data, ">>>>>>>>>>>>>>");
            return (
              <div>
                {/* <p>{item.name}</p> */}
                <a href="/add">{item.name} </a>

              </div>

            )
          })
        }

        <a href="/userDetail">View All User</a>
        {/* <a href="/userUpdate">Update User</a> */}
        {/* <a href="/userDelete">Delete User</a> */}
        <a href="/add">Add Course</a>
        <a href="/catadd">Add Category</a>
        <a href="/profile">MyProfile</a>

      </div>
    <div class="content pt-5">
        {/* <AddCategory /> */}
        <h4 class="pt-3 font-italic text-dark">Here are the List of all Courses..</h4>
        {/* <AddCategory /> */}
        <div class="container  text-center">
          <div class="row row-cols-1 row-cols-md-3 mt-3 mb-5 g-4">

            {
              list.map(item => {
                 console.log("gdghdfgsgf",item.id)
                return (
                  <div class="col">
                    <div class="card">
                      <div class="card-img-top">
                        {/* show image from server side */}
                        <p>{item.id}</p>
                        <img src={`http://94.237.3.78/demo_beta/${item.Image}`} alt="fftgh" style={{ width: "345px", height: "300px" }} />
                      </div>

                      <div class="card-body">
                        <h3 class="card-title font-weight-bolder font-italic text-break text-uppercase text-capitalize">{item.Title}</h3>
                        <h5 class="card-text font-weight-bolder ">{item.Description}</h5>
                        <h5>{item.duration}</h5>
                        <h6>{item.cost}</h6>
                        <p className='pl-0 text-dark'>{item.author}</p>
                        <br></br>
                        

                        <a href="/updateCourse">
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" href="/add" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                            <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
                          </svg>
                        </a>
                        <a href="#" onClick={() => coursedelete(item.id)}>
                          <span>&nbsp; &nbsp; &nbsp; &nbsp;</span>
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                            <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z" />
                          </svg>
                        </a>
                      </div>
                      <div class="mw-100 bg-primary card-footer">

                        <a href="#" class="btn btn-primary ">Enroll</a>
                      </div>

                    </div>
                  </div>

                )
              })
            }
          </div>
        </div>
        <Footer />
      </div>

    </div>


  )
}
export default AdminDashboard;

