import React, { Component } from 'react'
import axios from 'axios'
import { createUser } from '../Config/commonapi';


class Registration extends Component {
  handleSubmit = e => {

    e.preventDefault();

    const data = {
      data: this.data,

      roleId: this.roleId,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      phone: this.phone,
      password: this.password,

    }
    console.log('it works', data)

    axios.post('/createUser', data).then(


      res => {

        console.log(res)
      }
    ).catch(
      err => {
        console.log(err)
      }
    )
  }



  render() {
    console.log(createUser, " createUser")
    return (
      <div>

        <form className='m-5 text-center pt-5' onSubmit={this.handleSubmit}>
          <div className='container mx-auto'>
            <h4 className='pt-4'>Welcome To Registration Page</h4>
            <div className='form-group'>
              <br></br>

              <label>RoleId</label>
              <input type="text" placeholder='Enter RoleId' className='form-control'
                onChange={e => this.roleId = e.target.value} required />

            </div>
            <div className='form-group'>
              <br></br>

              <label>FirstName</label>
              <input type="text" placeholder='Enter FirstName' className='form-control'
                onChange={e => this.firstName = e.target.value} required />

            </div>
            <br></br>
            <div className='form-group'>

              <label>LastName</label>
              <input type="text" placeholder='Enter LastName ' className='form-control'
                onChange={e => this.lastName = e.target.value} required />

            </div>
            <br></br>
            <div className='form-group'>
              <label>Email</label>
              <input type="text" placeholder='Enter Email' className='form-control'
                onChange={e => this.email = e.target.value} required />

            </div>
            <br></br>
            <div className='form-group'>
              <label>Phone</label>
              <input type="number" placeholder='Enter Phone' className='form-control'
                onChange={e => this.phone = e.target.value} required />
            </div>
            <br></br>
            <div className='form-group'>
              <label>Password</label>
              <input type="password" placeholder='Enter Password' className='form-control'
                onChange={e => this.password = e.target.value} required />
            </div>
            <br />


            <a href="#" class="card-link text-dark float-right" style={{ paddingLeft: "530px", }}>
              You Have Already Register Please Login .!</a>



            <br></br>
            <div className='pt-3'>
              <button className="btn btn-primary ">USER CREATE HERE .!</button>
              {/* <a class="btn btn-outline-primary" href="/dash">Register Here .! </a> */}
              <span>&nbsp;</span>  <span>&nbsp; &nbsp;</span>
              {/* <a class="btn btn-outline-success" href="/login" type="submit">Back To Login </a> */}

            </div>
          </div>
        </form>
      
      </div>
    )

  }
}
export default Registration;
