import React, { Component } from 'react'
import axios from 'axios'
import '../Assest/Add.css';
import { UpdateCourse } from '../Config/commonapi';

class UpdateCours extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: [],
     // userId:null,
      Title: '',
      Description: '',
      Image: '',
       duration: '',
       cost: '',
       author: '',
       
    };
    // const data = {
    //   Title: this.Title,
    //   Description: this.Description,
    //   Image: this.state.image,
    //   duration: this.duration,
    //   cost: this.cost,
    //   author: this.author,
    //  // userId:this.userId,
    //     }
    this.update = this.update.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
  }
  componentDidMount() {
  
    fetch("Update_course/6244596bf896c8ef39be053b", {
      "method": "PUT",
      "headers": {
        "content-type": "application/json",
        "accept": "application/json"
      },
      "body": JSON.stringify({
        Title: this.state.Title,
        Description: this.state.Description,
        Image: this.state.Image,
        duration: this.state.duration,
        author: this.state.author,
        cost: this.state.cost,
      })
    })
    .then(response => response.json())
    .then(response => { console.log(response);
    })
    .catch(err => { console.log(err); });
        // const requestOptions = {
        //     method: 'PUT',
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify({ title: 'React PUT Request Example' })
        // };
        // fetch('Update_course/6244596bf896c8ef39be053b', requestOptions)
        //     .then(response => response.json())
        //     .then(data => this.setState({ userId: data.id }));
        //     console.log("daattttfty")
    }

 saveFile(e) {
    this.setState({ image: e.target.files[0] })
  }

 
  

  // handleSubmit = e => {
  //   e.preventDefault();
  //   let formData = new FormData();

  //   const data = {
  //     Title: this.Title,
  //     Description: this.Description,
  //     Image: this.state.image,
  //     duration: this.duration,
  //     cost: this.cost,
  //     author: this.author,
  //    // userId:this.userId,
  //       }
       
        
  //   for (var key in data) {
  // //    console.log(key, "Gggg", data[key])
  //     formData.append(key, data[key])
  //   }



  
  

      
  //   // axios.put('Update_course/6242bbdc6ac957629bc9fb9d', formData).then(
  //   //  // http://94.237.3.78:4000/api/Update_course/6242bbdc6ac957629bc9fb9d
  //   //   res => {
  //   //     console.log(res)
  //   //   }
  //   // ).catch(
  //   //   err => {
  //   //     console.log(err.response.data.message, "ffff")
  //   //   }
  // //  )
  // }
  handleChange(changeObject) {
    this.setState(changeObject)
  }

  render() {
     console.log(UpdateCourse, "UpdateCourse")
     const { userId } = this.state;
    return (

      <div>
        <form className='m-5 text-center pt-5' onSubmit={this.handleSubmit}>
          <div className='container mx-auto'>

            <div className='form-group'>
              <br></br>
              {userId }
              <label>Title</label>

              <div class="form-group">

                <select class="form-control" id="sel1"
                  value={this.state.Title}
                  onChange={(e) => this.handleChange({ Description: e.target.value })}>
                  {/* onChange={e => this.Title = e.target.value} > */}
                  <option value="JAVA">JAVA</option>
                  <option value="REACT"> REACT</option>
                  <option value="SCIENCE">SCIENCE</option>
                  <option value="MATHEMATICE">MATHEMATICE</option>
                  <option value="PCM">PCM</option>
                  <option value="CHEMISTRY">CHEMISTRY</option>
                  <option value="BIOLOGY">BIOLOGY</option>
                </select>
              </div>
             

            </div>
            <div className='form-group'>
              <br></br>

              <label>Description</label>
              <input type="text" placeholder='Enter Description' className='form-control'
                value={this.state.Description}
                onChange={(e) => this.handleChange({ Description: e.target.value })}/>
                {/* onChange={e => this.Description = e.target.value}  */}

            </div>
            <br></br>
            <div className='form-group'>

              <label>Image</label>
              
              <input type="file" onChange={(e) => this.saveFile(e)} name="image" />

            </div>
            <br></br>
            <div className='form-group'>
              <label>duration</label>
              <input type="text" placeholder='Enter duration' className='form-control'
               value={this.state.duration}
               onChange={(e) => this.handleChange({ duration: e.target.value })} />
                {/* onChange={e => this.duration = e.target.value} /> */}

            </div>
            <br></br>
            <div className='form-group'>
              <label>cost</label>
              <input type="number" placeholder='Enter cost' className='form-control'
                 value={this.state.cost}
                 onChange={(e) => this.handleChange({ cost: e.target.value })} />
                {/* onChange={e => this.cost = e.target.value} /> */}
            </div>
            <br></br>
            <div className='form-group'>
              <label>author</label>
              <input type="text" placeholder='Enter author' className='form-control'
                value={this.state.author}
                onChange={(e) => this.handleChange({ author: e.target.value })} />
                {/* onChange={e => this.author = e.target.value} /> */}
            </div>
            <br />
            

            <br></br>
            <div className='pt-3'>
            <button className="btn btn-info" type='button' onClick={(e) => this.update(e)}>Update </button>

              <button className="btn btn-primary">Update Course .!</button>
           </div>
          </div>
        </form>
        {/* <Footer /> */}
      </div>
    )
  }
}
export default UpdateCours;

