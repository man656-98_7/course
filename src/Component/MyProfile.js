import React, { useState, useEffect } from 'react'
import axios from 'axios';
import '../Assest/Banner.css';
import '../Assest/MyProfile.css';
import Footer from './Footer';
import { user_Detail } from '../Config/commonapi';

function MyProfile() {
    const [data, setData] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);
    async function fetchData() {
        await axios.get('/user_Detail/6242d889c8b03dd187589707')

            .then((response) => { setData(response.data) })
        //.then((response) => { console.log(response.data) });
    }
    console.log(user_Detail, "user_Detail/")

    return (
        <div>

            <div class="container">
                <div class="card pt-5">

                    <div class="card-body-md-4 pt-5">
                        <h4 class="text-center font-italic"><strong>“Happiness is not something ready made. It comes from your own actions.”</strong></h4>
                        <hr />
                        <div class="profile-card-4 text-center"><img src="http://envato.jayasankarkr.in/code/profile/assets/img/profile-4.jpg" class="img img-responsive" />
                            <div class="profile-content">
                                <div class="profile-name">
                                    {data.firstName}
                                    <p>FirstName</p>
                                </div>
                                <div class="profile-description">The Users page lists the users who are associated with your apps. To open a particular user, click the user profile picture or name in the "Name" column. The User Details page will open and display information for that user.

                                    .</div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="profile-overview">




                                            <p>LASTNAME</p>
                                            <h4>{data.lastName}</h4></div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="profile-overview">
                                            <p>PHONE</p>
                                            <h6>{data.phone}</h6>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="profile-overview">
                                            <p>FOLLOWING</p>
                                            <h6>{data.email}</h6></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
export default MyProfile;