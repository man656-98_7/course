import React from 'react';
import ReactDOM from 'react-dom';
//import App from './App';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from '../Component/Header';
import Footer from '../Component/Footer';
import Layout from './Layout';
import Banner from '../Component/Banner';
import SelfLearning from '../Component/SelfLearning';
import Login from '../Component/Login';
import Registration from '../Component/Registration';
import Dashboard from '../Component/Dashboard';
import AddCourse from '../Component/AddCourse';
import MyProfile from '../Component/MyProfile';
import Admin from '../Component/Admin';
import AdminDashboard from '../Component/AdminDashboard';
import Delete from '../Component/Delete';
import UpdateCours from '../Component/UpdateCours';
import AddCategory from '../Component/AddCategory';
import UserDetail from '../Component/UserDetail';
import Demo from '../Component/Demo';

 function Test(){

   return(
<BrowserRouter>
<Routes>
  {/* <Route path="/" element={<Layout />}> */}
  <Route path="/" element={<Layout />}>
    {/* <Route index element={<Header />} /> */}
    <Route path="/" index element={<Banner />} />
    <Route path="/registeration" element={<Registration />} />
    <Route path="/self" element={<SelfLearning />} />
    <Route path="/login" element={<Login />} />
    <Route path="/dash" element={<Dashboard />} />
    <Route path="/add" element={<AddCourse />} />
    <Route path="/admin" element={<Admin />} />
    <Route path="/admindash" element={<AdminDashboard />} />
    <Route path="/updateCourse" element={<UpdateCours />} />
    <Route path="/deleteCourse" element={<Delete />} />
    <Route path="/profile" element={<MyProfile />} />
     <Route path="/catadd" element={<AddCategory />} />
     <Route path="/userDetail" element={<UserDetail />} />
     <Route path="/demo" element={<Demo />} />
    {/* <Route path="*" element={<NoPage />} />  */}
  </Route>
</Routes>
</BrowserRouter>
   )
 }
export default Test;